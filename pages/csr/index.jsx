import React, { useEffect, useState } from 'react'
import { getFirestore, collection, getDocs } from "firebase/firestore";
import DefaultLayout from '../../layouts/WithNavLayout';
import firebaseApp from '../../utils/firebase';
import { useSelector } from 'react-redux';
import { useRouter } from 'next/router';

const csr = () => {
    const db = getFirestore(firebaseApp);
    const [Spaces, setSpaces] = useState([])
    const router = useRouter()
    const isAuthenticated = useSelector(state => state.auth.isAuthenticated) 

    useEffect(() => {
      if(!isAuthenticated)
        router.push('/login')
    }, [isAuthenticated])
    
    useEffect( () => {
      const getSpaceFromFS = []
      getDocs(collection(db, "spaces"))
      .then(
        data => {
          data.forEach(doc => getSpaceFromFS.push({...doc.data()}))
          setSpaces(getSpaceFromFS)
        }            
      )
    }, [])

  return (
    <div>CSR
        {Spaces.length > 0 && Spaces.map((space, index) => (
            <h1 key={index}>{space.name}</h1>
        ))}
    </div>
    
  )
}

csr.layout = (page) => (
  <DefaultLayout>
    {page}
  </DefaultLayout>
)

export default csr