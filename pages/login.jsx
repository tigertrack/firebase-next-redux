import React, {useState} from 'react'
import { Form, Button } from 'react-bootstrap'
import DefaultLayout from '../layouts/defaultLayout'
import { useDispatch } from 'react-redux'
import { login } from '../redux/reducers/auth'
import { getAuth, signInWithEmailAndPassword, getIdToken } from "firebase/auth";
import { useRouter } from 'next/router'
import firebaseApp from '../utils/firebase'

const Login = () => {
  const dispatch = useDispatch()
  const [Email, setEmail] = useState('')
  const [Password, setPassword] = useState('')
  const [Loading, setLoading] = useState(false)
  const router = useRouter()

  const handleLogin = () => {
    setLoading(true)
    const auth = getAuth(firebaseApp);
    signInWithEmailAndPassword(auth, Email, Password)
    .then(async credential => {
      const uidToken = await getIdToken(credential.user)
      console.log(credential.user)
      await dispatch(login({uidToken}))
      setLoading(true)
      router.push('/ssr')
    })
    .catch((error) => {
      console.log(`error code: ${error.code}\n ${error.message}`)
    });
    
  }
  return (
    <Form className="mt-5">
        <Form.Group className="mb-3" controlId="formBasicEmail">
            <Form.Label>Email address</Form.Label>
            <Form.Control type="email" placeholder="Enter email" onChange={(e) => setEmail(e.target.value)} />
            <Form.Text className="text-muted">
            We'll never share your email with anyone else.
            </Form.Text>
        </Form.Group>

        <Form.Group onKeyDown={(event) => event.key === 'Enter' && handleLogin()} className="mb-3" controlId="formBasicPassword">
            <Form.Label>Password</Form.Label>
            <Form.Control type="password" placeholder="Password" onChange={(e) => setPassword(e.target.value)} />
        </Form.Group>
        <Button disabled={Loading} variant="primary" type="button" onClick={handleLogin}>
            {Loading ? 'Loading...' : 'Submit'}
        </Button>
    </Form>
  )
}

Login.layout = (page) => (
    <DefaultLayout>
        {page}
    </DefaultLayout>
)
 
export default Login