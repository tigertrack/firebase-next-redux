import React from 'react'
import { getFirestore, collection, getDocs } from "firebase/firestore";
import DefaultLayout from '../../layouts/WithNavLayout';
import nookies from 'nookies'
import firebaseApp from '../../utils/firebase';

const ssr = ({Spaces}) => {
  
  return (
    <div>SSR
        {Spaces.length > 0 && Spaces.map((space, index) => (
            <h1 key={index}>{space.name}</h1>
        ))}
    </div>
    
  )
}

ssr.layout = (page) => (
  <DefaultLayout>
    {page}
  </DefaultLayout>
)

export default ssr

export async function getServerSideProps(ctx) {
    try {
      const cookies = nookies.get(ctx);
    
      const {firebaseAdmin} = require('../../utils/firebaseAdmin')

      const token = await firebaseAdmin.auth().verifyIdToken(cookies.token);

      const db = getFirestore(firebaseApp);
      const Spaces = []
      const data = await getDocs(collection(db, "spaces"))

      data.forEach(doc => Spaces.push({...doc.data()}))
        
      return {
        props: {Spaces}
      }
    } catch (error) {
      console.log(error)
      nookies.destroy(ctx, 'token')
      ctx.res.writeHead(302, { Location: '/login' });
      ctx.res.end();
      
    }
  }
  