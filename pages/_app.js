import '../styles/globals.css'
import 'bootstrap/dist/css/bootstrap.min.css';
import { store } from '../redux/store'
import { Provider } from 'react-redux'
function MyApp({ Component, pageProps }) {
  const pageLayout = Component.layout || ((page) => page);
  return (
    <Provider store={store}>
      {pageLayout(<Component {...pageProps} />)}
    </Provider>
  )
}

export default MyApp
