import React, {useEffect} from 'react'
import { getFirestore, collection, getDocs } from "firebase/firestore";
import DefaultLayout from '../../layouts/WithNavLayout';
import firebaseApp from '../../utils/firebase';
import { useSelector } from 'react-redux';
import { useRouter } from 'next/router';

const ssg = ({Spaces}) => {
  const router = useRouter()
  const isAuthenticated = useSelector(state => state.auth.isAuthenticated) 
  useEffect(() => {
    if(!isAuthenticated)
      router.push('/login')
  }, [])
  
  return (
    <div>SSG
        {Spaces.length > 0 && Spaces.map((space, index) => (
            <h1 key={index}>{space.name}</h1>
        ))}
    </div>
    
  )
}

ssg.layout = (page) => (
  <DefaultLayout>
    {page}
  </DefaultLayout>
)

export default ssg

export async function getStaticProps(ctx) {
  console.log(ctx)
    const db = getFirestore(firebaseApp);
    const Spaces = []
    const data = await getDocs(collection(db, "spaces"))

    data.forEach(doc => Spaces.push({...doc.data()}))

    return {
      props: {Spaces}
    }
  }
  