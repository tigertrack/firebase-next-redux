import React from 'react'
import Link from 'next/link'
import { Nav } from 'react-bootstrap'
const NextBSLink = ({href, name}) => {
  return (
      <Link href={href} passHref>
          <Nav.Link>{name}</Nav.Link>
      </Link>
  )
}

export default NextBSLink