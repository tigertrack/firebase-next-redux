import { createSlice } from '@reduxjs/toolkit'
import nookies from 'nookies'
const initialState = {
  isAuthenticated: false,
  user: null
}

export const auth = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    login: (state, action) => {
      state.isAuthenticated = true
      state.user = action.payload
      nookies.set(null, 'token', action.payload.uidToken, { path: '/' });
    },
    logout: (state) => {
      state.isAuthenticated = false
      nookies.destroy(null, 'token')
    },
  },
})

// Action creators are generated for each case reducer function
export const { login, logout } = auth.actions

export default auth.reducer