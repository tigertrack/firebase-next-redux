import React from 'react'
import { Navbar, Container, Nav } from 'react-bootstrap'
import NextBSLink from '../components/NextBSLink'
import { useDispatch } from 'react-redux'
import { logout } from '../redux/reducers/auth'
import { useRouter } from 'next/router'
const WithNavLayout = ({children}) => {
  const router = useRouter()
  const dispatch = useDispatch() 
  const handleLogout = () => {
    dispatch(logout())
    router.push('/login')
  }
  return (
    <div>
        <Navbar bg="light" expand="lg">
            <Container>
                <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto">
                    <NextBSLink href="/csr" name="CSR" />
                    <NextBSLink href="/ssr" name="SSR" />
                    <NextBSLink href="/ssg" name="SSG" />
                </Nav>
                <Nav className="ms-auto">
                    <Nav.Link name="" onClick={handleLogout}>Logout</Nav.Link>
                </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
        <Container>
        {children}
        </Container>

    </div>
  )
}

export default WithNavLayout